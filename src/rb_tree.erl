-module(rb_tree).

-export([insert/1, insert/2, search/2, pretty_print/1, delete/2]).
-export([t1/0, t2/0, test/0]).

-record(node, {color, key, left = undefined, right = undefined}).

-type rb_tree() ::
    #node{color :: red | black,
          key :: integer,
          left :: rb_tree(),
          right :: rb_tree()} |
    undefined.

%% @doc Searches for the Key in the Tree and prints out if the Tree contains Key or not
-spec search(Tree, Key) -> Result
    when Tree :: rb_tree(),
         Result :: rb_tree(),
         Key :: integer().
search(Tree, Key) ->
    search(Tree, Key, []),
    Tree.

%% @doc Tries to delete the Key in the Tree, if the Key is not in the tree returns unchanged Tree
-spec delete(Tree, Key) -> Result
    when Tree :: rb_tree(),
         Result :: rb_tree,
         Key :: integer().
delete(Tree, Key) ->
    black_root(delete_impl(Tree, Key)).

%% @doc Creates the red black tree and inserts the root key
-spec insert(Key) -> Result
    when Key :: integer(),
         Result :: rb_tree().
insert(Key) ->
    insert(undefined, Key).

%% @doc Inserts the key in the Tree, but the Tree can not contain duplicates
-spec insert(Tree, Key) -> Result
    when Tree :: rb_tree(),
         Key :: integer(),
         Result :: rb_tree().
insert(undefined, Key) ->
    black_node(Key);
insert(Tree, Key) ->
    black_root(insert_impl(Tree, red_node(Key))).

%% @doc Prints out the Tree
-spec pretty_print(Tree) -> true | false when Tree :: rb_tree().
pretty_print(Tree = #node{}) ->
    ok = pretty_print_impl(Tree, ""),
    Tree.

%% TREE PRINTING

pretty_print_impl(undefined, _Space) ->
    ok;
pretty_print_impl(#node{color = C,
                        key = Key,
                        right = R,
                        left = L},
                  Space) ->
    pretty_print_impl(R, Space ++ "         "),
    io:format(Space ++ "~p ~p~n", [C, Key]),
    pretty_print_impl(L, Space ++ "         ").

%% BIG ONES IMPL

search(undefined, _Key, _) ->
    false;
search(#node{key = Key}, Key, Path) ->
    io:format("Path >> ~p~nSearched Key >> ~p", [lists:reverse([Key | Path]), Key]),
    true;
search(#node{key = NodeKey, right = RightBranch}, Key, Path) when Key > NodeKey ->
    search(RightBranch, Key, [NodeKey | Path]);
search(#node{key = NodeKey, left = LeftBranch}, Key, Path) when Key < NodeKey ->
    search(LeftBranch, Key, [NodeKey | Path]).

delete_impl(undefined, Key) ->
    io:format("Key >> ~p does not exist~n", [Key]),
    undefined;
delete_impl(#node{key = Key,
                  left = undefined,
                  right = undefined},
            Key) ->
    undefined;
delete_impl(#node{key = Key,
                  left = LL,
                  right = undefined} =
                T,
            Key) ->
    {Suc, K} = highest_low(LL),
    RB = delete(LL, K),
    Suc#node{left = RB, right = T#node.right};
delete_impl(#node{key = Key, right = RR} = T, Key) ->
    {Suc, K} = lowest_high(RR),
    RB = delete(T, K),
    Suc#node{right = RB#node.right, left = T#node.left};
%% crveni
delete_impl(#node{left =
                      #node{key = Key,
                            color = red,
                            right = undefined,
                            left = undefined}} =
                Tree,
            Key) ->
    Tree#node{left = undefined};
delete_impl(#node{right =
                      #node{key = Key,
                            color = red,
                            right = undefined,
                            left = undefined}} =
                Tree,
            Key) ->
    Tree#node{right = undefined};
delete_impl(#node{left =
                      #node{key = Key,
                            left = undefined,
                            right = undefined}} =
                T,
            Key) ->
    deleted_left(T#node{left = undefined});
delete_impl(#node{left =
                      #node{key = Key,
                            left = LL,
                            right = undefined}} =
                T,
            Key) ->
    {Suc, K} = highest_low(LL),
    RB = delete(LL, K),
    Tree = T#node{left = Suc#node{right = RB}},
    deleted_left(Tree);
delete_impl(#node{left = #node{key = Key, right = LR}} = T, Key) ->
    {Suc, K} = lowest_high(LR),
    RB = delete(LR, K),
    Tree = T#node{left = Suc#node{right = RB}},
    deleted_left(Tree);
delete_impl(#node{right =
                      #node{key = Key,
                            left = undefined,
                            right = undefined}} =
                T,
            Key) ->
    deleted_right(T#node{right = undefined});
delete_impl(#node{right =
                      #node{key = Key,
                            left = RL,
                            right = undefined}} =
                T,
            Key) ->
    {Suc, K} = highest_low(RL),
    RB = delete(RL, K),
    Tree = T#node{right = Suc#node{right = RB}},
    deleted_right(Tree);
delete_impl(#node{right = #node{key = Key, right = RR}} = T, Key) ->
    {Suc, K} = lowest_high(RR),
    RB = delete(RR, K),
    Tree = T#node{right = Suc#node{right = RB}},
    deleted_right(Tree);
delete_impl(#node{key = _,
                  right = undefined,
                  left = undefined} =
                Tree,
            Key) ->
    io:format("Key >> ~p does not exist~n", [Key]),
    Tree;
delete_impl(#node{key = NodeKey, right = Right} = Tree, Key) when Key > NodeKey ->
    R = delete_impl(Right, Key),
    Tree#node{right = R};
delete_impl(#node{key = NodeKey, left = Left} = Tree, Key) when Key < NodeKey ->
    L = delete_impl(Left, Key),
    Tree#node{left = L}.

insert_impl(#node{key = Key} = Tree, #node{key = Key}) ->
    io:format("Duplicate key >> ~p~n", [Key]),
    Tree;
insert_impl(#node{key = NodeKey, right = undefined} = Tree, #node{key = Key} = NewNode)
    when NodeKey < Key ->
    Tree#node{right = NewNode};
insert_impl(#node{key = NodeKey, left = undefined} = Tree, #node{key = Key} = NewNode)
    when NodeKey > Key ->
    Tree#node{left = NewNode};
insert_impl(#node{key = NodeKey,
                  right = RightBranch,
                  left = LeftBranch} =
                Tree,
            #node{key = Key} = NewNode)
    when NodeKey < Key ->
    NewBranch = insert_impl(RightBranch, NewNode),
    T = Tree#node{right = NewBranch, left = LeftBranch},
    insert_color_fix(T);
insert_impl(#node{key = NodeKey,
                  left = LeftBranch,
                  right = RightBranch} =
                Tree,
            #node{key = Key} = NewNode)
    when NodeKey > Key ->
    NewBranch = insert_impl(LeftBranch, NewNode),
    T = Tree#node{left = NewBranch, right = RightBranch},
    insert_color_fix(T).

highest_low(T = #node{key = Key,
                      left = undefined,
                      right = undefined}) ->
    {T, Key};
highest_low(T = #node{key = Key, right = undefined}) ->
    {T, Key};
highest_low(#node{right = R}) ->
    highest_low(R).

lowest_high(T = #node{key = Key,
                      left = undefined,
                      right = undefined}) ->
    {T, Key};
lowest_high(T = #node{key = Key, left = undefined}) ->
    {T, Key};
lowest_high(#node{left = L}) ->
    lowest_high(L).

deleted_left(#node{left = #node{color = red}} = T) ->
    T;
deleted_left(#node{left = undefined,
                   right = #node{color = black, right = #node{color = red}}} =
                 T) ->
    delete_rr_rotate(T);
deleted_left(#node{left = #node{color = black},
                   right = #node{color = black, right = #node{color = red}}} =
                 T) ->
    delete_rr_rotate(T);
deleted_left(#node{right =
                       #node{color = black,
                             left = #node{color = red},
                             right = #node{color = red}}} =
                 Tree) ->
    delete_rr_rotate(Tree);
deleted_left(#node{right = #node{color = black, left = #node{color = red}}} = Tree) ->
    delete_rl_rotate(Tree);
deleted_left(#node{right = #node{color = black}, color = red} = T) ->
    T#node{color = black};
deleted_left(#node{right = #node{color = red}} = T) ->
    delete_r(T);
deleted_left(T) ->
    T.

deleted_right(#node{right = #node{color = red}} = T) ->
    T;
deleted_right(#node{right = undefined,
                    left = #node{color = black, left = #node{color = red}}} =
                  T) ->
    delete_ll_rotate(T);
deleted_right(#node{right = #node{color = black},
                    left = #node{color = black, left = #node{color = red}}} =
                  T) ->
    delete_ll_rotate(T);
deleted_right(#node{left =
                        #node{color = black,
                              left = #node{color = red},
                              right = #node{color = red}}} =
                  Tree) ->
    delete_ll_rotate(Tree);
deleted_right(#node{left = #node{color = black, right = #node{color = red}}} = Tree) ->
    delete_lr_rotate(Tree);
deleted_right(#node{left = #node{color = black}, color = red} = T) ->
    T#node{color = black};
deleted_right(#node{left = #node{color = red}} = T) ->
    delete_l(T);
deleted_right(T) ->
    T.

insert_color_fix(#node{color = black,
                       left = #node{color = red, left = #node{color = red}},
                       right = #node{color = black}} =
                     Tree) -> %% LEFT LEFT
    insert_l_rotate(Tree);
insert_color_fix(#node{color = black,
                       left = #node{color = red, left = #node{color = red}},
                       right = undefined} =
                     Tree) -> %% LEFT LEFT
    insert_l_rotate(Tree);
insert_color_fix(#node{color = black,
                       left = #node{color = red, right = #node{color = red}},
                       right = #node{color = black}} =
                     Tree) -> %% lEFT rIGHT
    insert_lr_rotate(Tree);
insert_color_fix(#node{color = black,
                       left = #node{color = red, right = #node{color = red}},
                       right = undefined} =
                     Tree) ->    %% LEFT RIGHT
    insert_lr_rotate(Tree);
insert_color_fix(#node{color = black,
                       left = #node{color = black},
                       right = #node{color = red, right = #node{color = red}}} =
                     Tree) -> %% RIGHT RIGHT
    insert_r_rotate(Tree);
insert_color_fix(#node{color = black,
                       left = undefined,
                       right = #node{color = red, right = #node{color = red}}} =
                     Tree) -> %% RIGHT RIGHT
    insert_r_rotate(Tree);
insert_color_fix(#node{color = black,
                       left = #node{color = black},
                       right = #node{color = red, left = #node{color = red}}} =
                     Tree) -> %% RIGHT LEFT
    insert_rl_rotate(Tree);
insert_color_fix(#node{color = black,
                       left = undefined,
                       right = #node{color = red, left = #node{color = red}}} =
                     Tree) -> %% IRHGT LERFT
    insert_rl_rotate(Tree);
insert_color_fix(Tree =
                     #node{color = black,
                           left = #node{color = red} = L,
                           right = #node{color = red} = R}) ->
    Tree#node{color = red,
              left = L#node{color = black},
              right = R#node{color = black}};
insert_color_fix(Tree) ->
    Tree.

delete_r(#node{right = R, color = TC} = T) ->
    RL = R#node.left,
    NewT = T#node{right = RL, color = R#node.color},
    R#node{color = TC, left = NewT}.

delete_l(#node{left = L, color = TC} = T) ->
    LR = L#node.right,
    NewT = T#node{left = LR, color = L#node.color},
    L#node{color = TC, right = NewT}.

%% rotations

insert_lr_rotate(#node{left = L} = T) ->
    LR = L#node.right,
    LRL = LR#node.left,
    NewL = L#node{right = LRL},
    NewT = T#node{left = LR#node{left = NewL}},
    insert_l_rotate(NewT).

insert_rl_rotate(#node{right = R} = T) ->
    RL = R#node.left,
    RLR = RL#node.right,
    NewR = R#node{left = RLR},
    NewT = T#node{right = RL#node{right = NewR}},
    insert_r_rotate(NewT).

insert_r_rotate(#node{right = R, color = C} = T) ->
    RL = R#node.left,
    RC = R#node.color,
    R#node{left = T#node{right = RL, color = RC}, color = C}.

insert_l_rotate(#node{left = L, color = C} = T) ->
    LR = L#node.right,
    LC = L#node.color,
    L#node{right = T#node{left = LR, color = LC}, color = C}.

delete_lr_rotate(#node{left = L} = T) ->
    LR = L#node.right,
    NewL = L#node{right = LR#node.left},
    NewT = T#node{left = LR#node{left = NewL}},
    delete_ll_rotate(NewT).

delete_ll_rotate(#node{left = L} = T) ->
    LR = L#node.right,
    NewT = T#node{left = LR},
    LL = L#node.left,
    L#node{right = NewT, left = LL}.

delete_rl_rotate(#node{right = R} = T) ->
    RL = R#node.left,
    NewR = R#node{left = RL#node.right},
    NewT = T#node{right = RL#node{right = NewR}},
    delete_rr_rotate(NewT).

delete_rr_rotate(#node{right = R} = T) ->
    RL = R#node.left,
    NewT = T#node{right = RL},
    RR = R#node.right,
    R#node{left = NewT, right = RR#node{color = NewT#node.color}}.

%% HELPER FUNS
black_node(Key) ->
    #node{color = black, key = Key}.

red_node(Key) ->
    #node{color = red, key = Key}.

black_root(Node = #node{}) ->
    Node#node{color = black};
black_root(_) ->
    undefined.

test() ->
    Root = 40,
    Insert = [50, 35, 55],
    Search = [],
    Delete = [35],
    tree_test(Root, Insert, Search, Delete).

t1() ->
    Root = 30,
    Insert = [20, 40, 35, 50, 25, 55],
    Search = [],
    Delete = [30],
    tree_test(Root, Insert, Search, Delete).

t2() ->
    Root = 30,
    Insert = [20, 40, 35],
    Search = [6],
    Delete = [20],
    tree_test(Root, Insert, Search, Delete).

tree_test(Root, Insert, Search, Delete) ->
    T = insert(Root),
    Tree = lists:foldl(fun(InsertKey, Acc) -> insert(Acc, InsertKey) end, T, Insert),
    pretty_print(Tree),
    lists:foldl(fun(SearchKey, Acc) -> search(Acc, SearchKey) end, Tree, Search),
    pretty_print(lists:foldl(fun(DeleteKey, Acc) -> delete(Acc, DeleteKey) end,
                             Tree,
                             Delete)).
